﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EminProje.Startup))]
namespace EminProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
