﻿using EminProje.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EminProje.Controllers
{
    public class HomeController : Controller
    {
        private restoranEntities db = new restoranEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {

            var rezervasyonlar = db.rezervasyon.ToList();
            return View(rezervasyonlar);
        }
    }
}